from constrictor.domains import RangeDomain
from constrictor.objects import ObjectLoader
from constrictor.rules import Rule, RuleLoader


if __name__ == "__main__":
    # Example of dynamically instantiating a rule that works on Strings
    ol = ObjectLoader("./object_types.json", "objects.json")
    print()
