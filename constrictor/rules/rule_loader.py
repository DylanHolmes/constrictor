import json

from constrictor.objects import ObjectLoader
from constrictor.rules import Rule


class RuleLoader:
    def __init__(self, source: str, ol: ObjectLoader):
        self._ol = ol
        self._source = source
        self._rules = []

    def load(self, source: str = None):
        self._source = source if source else self._source

        rules = json.loads(open(self.source, "r").read())["rules"]
        self._rules = []
        for rule_name in rules:
            rule = rules[rule_name]
            cons = rule["constructors"]
            arg_type = rule["arg_type"]
            func = rule["function"]
            self._rules.append(Rule(cons, arg_type, func, self._ol))

    @property
    def rules(self):
        return self._rules

    @property
    def source(self):
        return self._source
