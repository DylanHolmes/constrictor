from pydoc import locate

from constrictor.domains import Domain
from constrictor.objects import ObjectLoader


class Rule:
    def __init__(self, cons: list, arg_type: str, function: str, object_loader: ObjectLoader):
        self.cons = cons

        if arg_type.startswith("\\"):
            self.arg_type = object_loader.get_object_by_name(arg_type.replace("\\", ""))
        else:
            self.arg_type = locate(arg_type)

        self.func = eval(self.replace_cons(function, self.cons))

    def replace_cons(self, equation: str, replacements: list):
        for i in range(len(replacements)):
            equation = equation.replace(f"\c{i}", str(replacements[i]))

        return equation

    def match(self, value):
        correct_type = (
            isinstance(value, self.arg_type)
            if type(self.arg_type) == type
            else self.arg_type.isinstance(value)
        )
        return correct_type and self.func(value)

    def sub_domain(self, domain: Domain):
        return Domain([obj for obj in domain if self.match(obj)])
