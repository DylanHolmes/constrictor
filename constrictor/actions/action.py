from typing import List
from constrictor.domains import Domain
from constrictor.rules import Rule


class Action:
    def __init__(self, domain: Domain, rules: List[Rule], obj) -> None:
        self._domain = domain
        self._rules = rules
        self._obj = obj

    @property
    def domain(self):
        return self._domain

    def perform(self):
        dom = self.domain
        for rule in self._rules:
            dom = rule.sub_domain(dom)

        return dom.objects
