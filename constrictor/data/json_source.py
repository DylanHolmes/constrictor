import json

from data_source import DataSource


class JSONSource(DataSource):
    def __init__(self, file_path: str):
        self._data = json.loads(open(file_path, "r").read())

    def get_objects(self):
        self._data["objects"]
