from abc import ABC, abstractmethod

from constrictor.objects import ObjectType


class DataSource(ABC):
    @abstractmethod
    def get_objects(self) -> ObjectType:
        ...
