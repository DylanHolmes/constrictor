class ObjectType:
    def __init__(self, name, attributes: dict, values: dict):
        self.name = name
        self.attrs = attributes
        self.values = values

    def isinstance(self, obj: dict):
        for k, v in self.attrs.items():
            if not (k in obj.keys() and type(obj[k]) == v):
                return False

        return True
