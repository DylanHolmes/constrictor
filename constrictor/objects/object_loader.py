import json
from pydoc import locate

from constrictor.objects import ObjectType


class ObjectLoader:
    def __init__(self, type_file: str, obj_file: str):
        types = json.loads(open(type_file, "r").read())
        self._types = {}
        self._objects = []
        for type in types:
            attrs = parse_attributes(type["attributes"])
            values = parse_values(type["values"])

            self._types[type["name"]] = ObjectType(type["name"], attrs, values)

        objs = json.loads(open(obj_file, "r").read())

        for obj in objs:
            type = obj["type"]
            if type not in self.types:
                raise ValueError(f"Defined Type ({type}) not in the defined Types!")
            type: ObjectType = self.types[type]
            attrs = obj["attributes"]
            if not type.isinstance(attrs):
                raise ValueError(
                    f"Not a valid definition of a {type.name} type! Missing/invalid attributes"
                )
            self._objects.append(attrs)

    def load(self, source: str = None):
        if source is not None:
            self._source = source

    @property
    def source(self):
        return self._source

    @property
    def types(self):
        return self._types

    @property
    def objects(self):
        return self._objects

    def get_object_by_name(self, name: str):
        for obj in self.objects:
            if obj.name == name:
                return obj

        return None


def parse_attributes(attrs: list):
    attributes: dict = {}

    for attr in attrs:
        name, type = attr.split(":")
        attributes[name.lower()] = locate(type.lower())

    return attributes


def parse_values(values: list):
    dict_values: dict = {}

    for val in values:
        name, value, type = val.split(":")
        dict_values[name.lower()] = locate(type.lower())(value)

    return dict_values
