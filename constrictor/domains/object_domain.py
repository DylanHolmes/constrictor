from constrictor.data import DataSource
from constrictor.domains import Domain
from constrictor.objects import ObjectType


class ObjectDomain(Domain):
    def __init__(self, type: ObjectType, src: DataSource):
        super().__init__(src.get_objects(type))
        self._type = type
        self._src = src
