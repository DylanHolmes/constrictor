from typing import List

from constrictor.domains.domain import Domain


class RangeDomain(Domain):
    def __init__(self, given_range):
        super().__init__(list(given_range))

    @staticmethod
    def float_range(start: float, end: float, increment: float, percision: int = 2):
        cur = start
        while cur <= end:
            yield round(cur, percision)
            cur += increment
