from abc import ABC
from typing import List


class Domain(ABC):
    def __init__(self, objects: list):
        self._objs = objects

    def __iter__(self):
        return iter(self.objects)

    @property
    def objects(self):
        return self._objs
